﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System.Threading;
using NUnit.Framework;
using System.IO;
using OpenQA.Selenium.Chrome;

namespace ImplementDemo
{
    class ProgramDemo
    {
        IWebDriver driver;

        [OneTimeSetUp]
        public void StartBrowser()
        {
            driver = new ChromeDriver();
        }

        [Category("Regression")]
        [Test]
        public void Test()
        {
            var searchFor = "Text";
            driver.Url = "http://www.google.co.in";
            var inputsearch = driver.FindElement(By.Name("q"));
            inputsearch.SendKeys(searchFor);
            inputsearch.SendKeys(Keys.Enter);
            

            int results = driver.FindElements(By.XPath("//div[@class='g']//h3[contains(text(),'"+searchFor.ToLower()+"')]")).Count();
            Assert.Greater(results,0);
 


        }

        [Test]
        public void Test2()
        {

        }

        [OneTimeTearDown]
        public void CloseBrowser()
        {
            driver.Close();
            driver.Dispose();
        }
    }
}
