﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SeleniumExtras.WaitHelpers;
using NUnit.Framework;

namespace ImplementDemo.PageObjects
{
    class GoogleHomePage
    {
        private IWebDriver driver;

        public GoogleHomePage(IWebDriver driver)
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);
        }

        [FindsBy(How = How.Name, Using = "q")]
        private IWebElement pInputGoogleSearch;
        
        String pResultHeader = "//h3[@class='LC20lb'][contains(text(),'$TEXT$')]";


        public void SearchInput(string search)
        {
            var inpGoogleSrch = driver.FindElementIWeb(pInputGoogleSearch,10);

            inpGoogleSrch.SendKeys(search);
            inpGoogleSrch.SendKeys(Keys.Enter);

            var pResHeader = pResultHeader.Replace("$TEXT$", search);
            var result = driver.FindElement(By.XPath(pResHeader),10);

        }

        public void goToPage()
        {
            driver.Navigate().GoToUrl("http://www.google.co.in");
        }

    }
}
